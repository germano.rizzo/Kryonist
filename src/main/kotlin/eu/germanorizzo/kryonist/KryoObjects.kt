/*
    This file is part of Kryonist

    Kryonist is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kryonist is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kryonist.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.germanorizzo.kryonist

import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream

data class KryoFile(val id: String, var name: String, val iv: ByteArray, val key: ByteArray,
                    val hash: ByteArray, val len: Long, var section: String, var subsection: String,
                    var notes: String) {
    companion object {
        val INVALID_NAMES = setOf("NUL", "CON", "PRN", "AUX")

        val TITLES = arrayOf("Enc. File", "File Name", "Length", "Section", "Subsection", "Notes")

        fun read(dis: DataInputStream, fileFormatVersion: Byte): KryoFile {
            with(dis) {
                val id = readUTF()

                val iv = ByteArray(readInt())
                read(iv)

                val key = ByteArray(readInt())
                read(key)

                val hash = ByteArray(readInt())
                read(hash)

                val name = readUTF()

                val len = readLong()

                val section = readUTF()

                val subsection = readUTF()

                val notes = if (fileFormatVersion == 0x01.toByte()) "" else readUTF()

                return KryoFile(id, name, iv, key, hash, len, section, subsection, notes)
            }
        }
    }

    val lenHR
        get() = GenUtils.formatSize(len)

    fun write(dos: DataOutputStream) {
        dos.apply {
            writeUTF(id)

            writeInt(iv.size)
            write(iv)

            writeInt(key.size)
            write(key)

            writeInt(hash.size)
            write(hash)

            writeUTF(name)

            writeLong(len)

            writeUTF(section)

            writeUTF(subsection)

            writeUTF(notes)
        }
    }
}

class KryoFiles private constructor(pwd: String,
                                    private val files: MutableList<KryoFile>) : MutableList<KryoFile> by files {
    constructor(pwd: String) : this(pwd, mutableListOf<KryoFile>())

    val krypto = Krypto(pwd)

    companion object {
        const val FILE_FORMAT_VERSION: Byte = 0x02

        fun getFile(parent: Path): Path = GenUtils.child(parent, Strings.ARCHIVE_FILE)

        fun read(pwd: String, root: Path): KryoFiles {
            val file = KryoFiles.getFile(root)

            require(Files.exists(file), { "File ${Strings.ARCHIVE_FILE} does not exists" })

            val myKrypto = Krypto(pwd)

            val files = mutableListOf<KryoFile>()
            BufferedInputStream(Files.newInputStream(file)).use { fis ->
                val fileFormatVersion = fis.read().toByte()
                require(fileFormatVersion <= FILE_FORMAT_VERSION, { "Unsupported metadata file format version. Please use the latest release of Kryonist" })

                val myIV = ByteArray(Krypto.IV_SIZE)
                fis.read(myIV)
                val myKeyHash = Krypto.calcSHA(myIV + myKrypto.key)
                val fileKeyHash = ByteArray(myKeyHash.size)
                fis.read(fileKeyHash)
                require(myKeyHash contentEquals fileKeyHash, {
                    Thread.sleep(2500) // Wait if password check fails
                    "The password might be wrong"
                })

                // Read all decrypted bytes in memory to avoid a strange bug for which using
                // directly a DataInputStream on a CipherInputStream would read data up to 512 bytes,
                // which is strange. 512 bytes, incidentally, is the buffer size of CipherInputStream
                val bytes = ByteArrayOutputStream().use { baos ->
                    CipherInputStream(fis, myKrypto.getCipher(Cipher.DECRYPT_MODE, myIV)).use {
                        it.copyTo(baos)
                    }
                    baos
                }.toByteArray()

                DataInputStream(ByteArrayInputStream(bytes)).use { dis ->
                    for (i in 1..dis.readInt())
                        files += KryoFile.read(dis, fileFormatVersion)
                }

                val ret = KryoFiles(pwd, files)

                if (fileFormatVersion != FILE_FORMAT_VERSION) {
                    ret.doBackup(root)
                    print("The current metadata file uses an outdated file format. Upgrading it... ")
                    ret.write(root)
                    println("Ok.")
                }

                return ret
            }
        }
    }

    tailrec fun getAvailableFilename(): String {
        val ret = GenUtils.getRandomFilename()
        if (ret !in KryoFile.INVALID_NAMES && find { f -> f.id == ret } == null)
            return ret
        return getAvailableFilename()
    }

    fun create(root: Path): KryoFiles {
        return KryoFiles.getFile(root).let { file ->
            require(!Files.exists(file), { "Metadata file already exists" })
            Files.createFile(file)
            writeCommons(file)
        }
    }

    fun write(root: Path): KryoFiles {
        return KryoFiles.getFile(root).let { file ->
            require(Files.exists(file), { "Metadata file does not exist" })
            writeCommons(file)
        }
    }

    fun doBackup(root: Path) {
        val file = getFile(root)
        val dest = Paths.get(file.parent.toString(), file.fileName.toString() + ".bak")
        if (Files.exists(dest))
            Files.delete(dest)
        Files.copy(file, dest)
    }

    private fun writeCommons(file: Path): KryoFiles {
        BufferedOutputStream(Files.newOutputStream(file,
                StandardOpenOption.TRUNCATE_EXISTING)).use { fos ->
            fos.write(FILE_FORMAT_VERSION.toInt()) // file format version
            val myIV = Krypto.genIV()
            fos.write(myIV)
            fos.write(Krypto.calcSHA(myIV + krypto.key))
            DataOutputStream(
                    CipherOutputStream(fos, krypto.getCipher(Cipher.ENCRYPT_MODE, myIV))).use { dos ->
                dos.writeInt(files.size)
                files.forEach { file -> file.write(dos) }
            }
        }
        return this
    }

    private fun KryoFile.fields() =
            arrayOf(this.id, this.name, this.lenHR, this.section, this.subsection, this.notes)

    fun asString(filter: (KryoFile) -> Boolean): String {
        // Some utility functions

        fun StringBuilder.appendRPadded(str: String, pad: Int, char: Char = ' ') {
            this.append(str)
            this.append(char.toString().repeat(pad - str.length))
        }

        // Computation of the maximum required length of each field
        val lengths = IntArray(KryoFile.TITLES.size) { KryoFile.TITLES[it].length }
        val filtered = files.filter(filter)

        filtered.forEach { it.fields().forEachIndexed { i, s -> lengths[i] = Math.max(lengths[i], s.length) } }

        val ret = StringBuilder()

        // Add the title line
        KryoFile.TITLES.forEachIndexed { i, s -> ret.appendRPadded(s, lengths[i] + 3) }
        ret.append('\n')

        // Separator line
        lengths.forEach { i ->
            ret.appendRPadded("", i + 2, '=')
            ret.append(' ')
        }
        ret.append('\n')

        // Files, sorted by section -> subsection -> file name -> encrypted file name
        filtered.sortedWith(compareBy({ it.section }, { it.subsection }, { it.name }, { it.id }))
                .forEach {
                    it.fields().forEachIndexed { i, s -> ret.appendRPadded(" " + s, lengths[i] + 3) }
                    ret.append('\n')
                }

        return ret.toString()
    }

    fun exportToWriter(wr: Writer) {
        // titles
        wr.write(KryoFile.TITLES.joinToString("\t"))
        wr.write(System.lineSeparator())

        // Files, sorted by section -> subsection -> file name -> encrypted file name
        files.sortedWith(compareBy({ it.section }, { it.subsection }, { it.name }, { it.id }))
                .forEach {
                    wr.write(it.fields().joinToString("\t"))
                    wr.write(System.lineSeparator())
                }
    }
}
