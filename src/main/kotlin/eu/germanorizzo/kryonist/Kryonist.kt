/*
    This file is part of Kryonist

    Kryonist is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kryonist is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kryonist.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.germanorizzo.kryonist

import eu.germanorizzo.kryonist.menu.TextMenu
import eu.germanorizzo.kryonist.menu.TextMenuDisplayHelpWhen
import eu.germanorizzo.kryonist.menu.TextMenuItem
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.security.DigestInputStream
import java.security.DigestOutputStream
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream

object Strings {
    const val VERSION = "0.3.2"

    const val ENC_DIR = "enc"
    const val DEC_DIR = "dec"

    const val ARCHIVE_FILE = "archive.kryo"
    const val EXPORT_FILE = "archive-export.csv"

    // short commands
    const val SC_ENCODE = 'e'
    const val SC_DECODE = 'd'
    const val SC_LIST = 'l'
    const val SC_SAVE = 's'
    const val SC_METADATA = 'm'
    const val SC_DELETE = 'x'
    const val SC_VERIFY = 'v'

    const val USAGE = "Usage: java -jar kryonist.jar <path_to_env_dir>"
}

fun main(args: Array<String>) {
    println("Kryonist v${Strings.VERSION}")
    println("(c) 2018 by G.R. under the terms of GPLv3")
    println()

    if (!Krypto.isDigestSupported())
        ConsoleAppUtils.exitWithError("ERROR: Cannot find any provider supporting SHA-256")

    if (!Krypto.isCipherSupported())
        ConsoleAppUtils.exitWithError(
                "ERROR: Cannot find any provider supporting AES with ${Krypto.KEY_SIZE shl 3}-bit keys and ${Krypto.IV_SIZE shl 3}-bit IV")

    if (args.size != 1)
        ConsoleAppUtils.exitWithError(Strings.USAGE)

    val path = Paths.get(args[0])

    val archive = if (!Files.exists(KryoFiles.getFile(path))) {
        println("No environment found. Initializing...")
        val archive = doInit(path)
        println("Done.")
        archive
    } else {
        tailrec fun askPass(): KryoFiles = try {
            val pwd = ConsoleAppUtils.askPassword()
            val ret = KryoFiles.read(pwd, path) // also checks that the password is correct
            println("Password Ok.")
            ret
        } catch (e: IllegalArgumentException) {
            println("ERROR: ${e.message}")
            null
        } ?: askPass()

        askPass()
    }
    println()

    GenUtils.createDirIfNotExists(path, Strings.ENC_DIR)
    GenUtils.createDirIfNotExists(path, Strings.DEC_DIR)

    TextMenu(
            TextMenuItem(Strings.SC_LIST,
                    { "print a list of the entire archive" },
                    { doList(archive); false }),
            TextMenuItem(Strings.SC_SAVE,
                    { "saves the list in a tab-delimited plain text file named '${Strings.EXPORT_FILE}'" },
                    { doSave(archive, path); false }),
            TextMenuItem(Strings.SC_ENCODE,
                    { "encodes everything in the '${Strings.DEC_DIR}' folder to the '${Strings.ENC_DIR}' folder" },
                    { doEncode(archive, path); false }),
            TextMenuItem(Strings.SC_DECODE,
                    { "decodes everything in the '${Strings.ENC_DIR}' folder to the '${Strings.DEC_DIR}' folder" },
                    { doDecode(archive, path); false }),
            TextMenuItem(Strings.SC_METADATA,
                    { "edit metadata of a set of files" },
                    { doMetadata(archive, path); false }),
            TextMenuItem(Strings.SC_DELETE,
                    { "deletes a set of files from the archive" },
                    { doDelete(archive, path); false }),
            TextMenuItem(Strings.SC_VERIFY,
                    { "verifies the integrity of the files in the '${Strings.ENC_DIR}' folder" },
                    { doVerify(archive, path); false }),
            onException = { e -> println("Error: ${e.message}") },
            headerForHelp = { "Available commands:" }
    ).run()

    println("See you soon!")
}

private fun doInit(pathToEnvDir: Path): KryoFiles {
    val pwd = ConsoleAppUtils.askPassword()
    require(pwd.length >= 12, { "Password should be at least 12 characters" })

    val pwd2 = ConsoleAppUtils.askPassword("Repeat")
    require(pwd == pwd2, { "Passwords do not match" })

    return KryoFiles(pwd).create(pathToEnvDir)
}

private fun doEncode(archive: KryoFiles, root: Path) {
    val (section, subsection, notes) =
            if (ConsoleAppUtils.askConfirmation("Do you want to specify metadata for the files to encrypt?"))
                Triple(ConsoleAppUtils.askString("Section to assign"),
                        ConsoleAppUtils.askString("Subsection to assign"),
                        ConsoleAppUtils.askString("Notes to assign"))
            else
                Triple("", "", "")

    val delete = ConsoleAppUtils.askConfirmation("Do you want to delete the files after encoding them?")

    println("Performing a backup...")
    archive.doBackup(root)

    val dir = GenUtils.child(root, Strings.DEC_DIR)
    val dest = GenUtils.child(root, Strings.ENC_DIR)

    Files.list(dir).filter { Files.isRegularFile(it) }.forEach { fileToDec ->
        val encName = archive.getAvailableFilename()

        val myIV = Krypto.genIV()
        val myKey = Krypto.getRandomBytes(Krypto.KEY_SIZE)
        val fileToEnc = GenUtils.child(dest, encName)

        print("Encoding '${fileToDec.fileName}' to '$encName'... ")

        lateinit var hash: ByteArray
        DigestInputStream(
                BufferedInputStream(Files.newInputStream(fileToDec)), Krypto.getDigest()).use { his ->
            BufferedOutputStream(Files.newOutputStream(fileToEnc)).use { bos ->
                bos.write(0x01) // file format version
                CipherOutputStream(bos,
                        archive.krypto.getCipher(Cipher.ENCRYPT_MODE, myIV, myKey)).use { cos ->
                    his.copyTo(cos)
                    hash = his.messageDigest.digest()
                }
            }
        }

        println("Ok.")

        val encFile = KryoFile(encName, fileToDec.fileName.toString(), myIV,
                myKey, hash, Files.size(fileToDec),
                section, subsection, notes)

        archive.add(encFile)
        archive.write(root)

        if (delete)
            Files.delete(fileToDec)
    }
}

private fun doDecode(archive: KryoFiles, root: Path) {
    val delete = ConsoleAppUtils.askConfirmation("Do you want to delete the files after decoding them?")

    val dir = GenUtils.child(root, Strings.ENC_DIR)
    val dest = GenUtils.child(root, Strings.DEC_DIR)

    Files.list(dir).filter { Files.isRegularFile(it) }.forEach { fileToEnc ->

        // verify that the file is in the archive
        val record = archive.find { it.id == fileToEnc.fileName.toString() }
                ?: throw IllegalStateException("File record not found for '${fileToEnc.fileName}'")

        print("Decoding '${fileToEnc.fileName}' to '${record.name}'... ")

        val decFile = Paths.get(dest.toString(), record.name)
        val hash = BufferedInputStream(Files.newInputStream(fileToEnc)).use { bis ->
            require(bis.read() == 0x01, { "Wrong data file format version" })
            CipherInputStream(bis,
                    archive.krypto.getCipher(Cipher.DECRYPT_MODE, record.iv, record.key)).use { cis ->
                DigestOutputStream(
                        BufferedOutputStream(Files.newOutputStream(decFile)), Krypto.getDigest()).use { hos ->
                    cis.copyTo(hos)
                    hos.messageDigest.digest()
                }
            }
        }

        if (!(hash contentEquals record.hash))
            println("WARNING: The file '${record.name}' seems corrupted")
        else {
            if (delete)
                Files.delete(fileToEnc)

            println("Ok.")
        }
    }
}

private fun doVerify(archive: KryoFiles, root: Path) {
    val dir = GenUtils.child(root, Strings.ENC_DIR)

    Files.list(dir).filter { Files.isRegularFile(it) }.forEach { fileToEnc ->

        // verify that the file is in the archive
        val record = archive.find { it.id == fileToEnc.fileName.toString() }
                ?: throw IllegalStateException("File record not found for '${fileToEnc.fileName}'")

        print("Verifying '${fileToEnc.fileName}'... ")

        val hash = BufferedInputStream(Files.newInputStream(fileToEnc)).use { bis ->
            require(bis.read() == 0x01, { "Wrong data file format version" })
            CipherInputStream(bis,
                    archive.krypto.getCipher(Cipher.DECRYPT_MODE, record.iv, record.key)).use { cis ->
                DigestOutputStream(NullOutputStream(), Krypto.getDigest()).use { hos ->
                    cis.copyTo(hos)
                    hos.messageDigest.digest()
                }
            }
        }

        if (!(hash contentEquals record.hash))
            println("WARNING: The file '${record.name}' seems corrupted")
        else
            println("Ok.")
    }
}

fun doSave(archive: KryoFiles, root: Path) {
    val expFile = Paths.get(root.toString(), Strings.EXPORT_FILE)
    Files.newBufferedWriter(expFile, Charset.forName("UTF-8")).use(archive::exportToWriter)
    println("Ok")
}

fun doMetadata(archive: KryoFiles, root: Path) {
    val files = askEncFiles(archive)

    if (files.isEmpty())
        return

    println("Performing a backup...")
    archive.doBackup(root)

    if (files.size == 1)
        doMetadataSingle(files[0])
    else
        doMetadataMultiple(files)

    archive.write(root)
    println("Ok")
}

private fun askEncFiles(archive: KryoFiles) =
        mutableListOf<KryoFile>().also { files ->
            val fNames = ConsoleAppUtils.askString("List of encrypted file names, separated with spaces")
            if (!fNames.isBlank())
                fNames.split(" ").forEach { file ->
                    files += archive.find { it.id == file.toUpperCase() }
                            ?: throw IllegalArgumentException("Some of the names are not valid")
                }
        }

private fun doMetadataSingle(file: KryoFile) {
    fun ask() = ConsoleAppUtils.askString("Enter the new value")

    TextMenu(
            TextMenuItem('1', { "File name (\"${file.name}\")" }, { file.name = ask(); false }),
            TextMenuItem('2', { "Section (\"${file.section}\")" }, { file.section = ask(); false }),
            TextMenuItem('3', { "Subsection (\"${file.subsection}\")" }, { file.subsection = ask(); false }),
            TextMenuItem('4', { "Notes (\"${file.notes}\")" }, { file.notes = ask(); false }),
            onException = {},
            tmdhw = TextMenuDisplayHelpWhen.ALWAYS,
            headerForHelp = { "What do you want to edit?" }
    ).run()
}

private fun doMetadataMultiple(files: MutableList<KryoFile>) {
    fun getValue(field: (KryoFile) -> String): String {
        val values = mutableSetOf<String>()
        files.forEach { values += field(it) }
        return when (values.size) {
            0 -> ""
            1 -> values.first()
            else -> "multiple values"
        }
    }

    fun setValue(setter: (KryoFile, String) -> Unit) {
        val value = ConsoleAppUtils.askString("Enter the new value")
        files.forEach { setter(it, value) }
    }

    TextMenu(
            TextMenuItem('1',
                    { "Section (\"${getValue { it.section }}\")" },
                    { setValue({ file, str -> file.section = str }); false }),
            TextMenuItem('2',
                    { "Subsection (\"${getValue { it.subsection }}\")" },
                    { setValue({ file, str -> file.subsection = str }); false }),
            TextMenuItem('3',
                    { "Notes (\"${getValue { it.notes }}\")" },
                    { setValue({ file, str -> file.notes = str }); false }),
            onException = { throw it },
            tmdhw = TextMenuDisplayHelpWhen.ALWAYS,
            headerForHelp = { "What do you want to edit?" }
    ).run()
}

fun doDelete(archive: KryoFiles, root: Path) {
    val files = askEncFiles(archive)

    if (files.isEmpty())
        return

    println("Performing a backup...")
    archive.doBackup(root)

    archive.removeAll(files)

    archive.write(root)
    println("Ok")
}

fun doList(archive: KryoFiles) {
    TextMenu(
            TextMenuItem('1',
                    { "List all items" },
                    {
                        println(archive.asString({ true }))
                        true
                    }),
            TextMenuItem('2',
                    { "List items by Section/Subsection" },
                    {
                        doListBySection(archive)
                        true
                    }),
            TextMenuItem('3',
                    { "List items by Keywords" },
                    {
                        doListByKeyword(archive)
                        true
                    }),
            onException = { throw it },
            tmdhw = TextMenuDisplayHelpWhen.ALWAYS,
            headerForHelp = { "Choose a filtering mode." }
    ).run()
}

private fun doListBySection(archive: KryoFiles) {
    fun String.ifEmpty() = if (this.isEmpty()) "<empty>" else this

    fun print(section: String, subsection: String?) {
        println(if (subsection == null)
            archive.asString({ it.section == section })
        else
            archive.asString({ it.section == section && it.subsection == subsection }))
    }

    fun chooseSubsection(section: String) {
        val subsections = TreeSet<String>()
        archive.filter { it.section == section }.forEach { subsections += it.subsection }
        val secItems =
                mutableListOf(TextMenuItem('*',
                        { "All Subsections" },
                        {
                            print(section, null)
                            true
                        }))
        subsections.forEachIndexed { i, it ->
            secItems += TextMenuItem('1' + i,
                    { it.ifEmpty() },
                    {
                        print(section, it)
                        true
                    })
        }
        TextMenu(*secItems.toTypedArray(),
                onException = { throw it },
                tmdhw = TextMenuDisplayHelpWhen.ALWAYS,
                headerForHelp = { "Choose a Subsection." }
        ).run()
    }

    val sections = TreeSet<String>()
    archive.forEach { sections += it.section }
    val secItems = mutableListOf<TextMenuItem>()
    sections.forEachIndexed { i, it ->
        secItems += TextMenuItem('1' + i,
                { it.ifEmpty() },
                {
                    chooseSubsection(it)
                    true
                })
    }
    TextMenu(*secItems.toTypedArray(),
            onException = { throw it },
            tmdhw = TextMenuDisplayHelpWhen.ALWAYS,
            headerForHelp = { "Choose a Section." }
    ).run()
}

private fun doListByKeyword(archive: KryoFiles) {
    val keywords = ConsoleAppUtils.askString("Keywords (separated by spaces): ")
            .toLowerCase().split(" ")
    if (keywords.isEmpty())
        return

    val filter = filter@ { it: KryoFile ->
        keywords.forEach { word ->
            if (word in it.name.toLowerCase()
                    || word in it.section.toLowerCase()
                    || word in it.subsection.toLowerCase()
                    || word in it.notes.toLowerCase()
                    || word == it.id.toLowerCase()) // look also in the file ID, but as precise match
                return@filter true
        }
        return@filter false
    }

    println(archive.asString(filter))
}