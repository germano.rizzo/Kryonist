package eu.germanorizzo.kryonist.menu

import java.io.BufferedReader
import java.io.InputStreamReader

data class TextMenuItem(val opCode: Char, val text: () -> String, val action: () -> Boolean)

enum class TextMenuDisplayHelpWhen {
    ALWAYS, FIRST_TIME, NEVER
}

class TextMenu(private vararg val items: TextMenuItem,
               private val onException: (Exception) -> Unit,
               private val tmdhw: TextMenuDisplayHelpWhen = TextMenuDisplayHelpWhen.FIRST_TIME,
               private val headerForHelp: () -> String? = { null }) {
    companion object {
        private const val HELP_CMD = 'h'
        private const val QUIT_CMD = 'q'
    }

    init {
        require(items.find { HELP_CMD == it.opCode } == null, { "$HELP_CMD is a reserved operation code" })
        require(items.find { QUIT_CMD == it.opCode } == null, { "$QUIT_CMD is a reserved operation code" })
    }

    fun run() {
        tailrec fun run(isFirst: Boolean) {
            println()
            if (tmdhw == TextMenuDisplayHelpWhen.ALWAYS || (isFirst && tmdhw == TextMenuDisplayHelpWhen.FIRST_TIME)) {
                printHelp()
            }
            print("Please choose an operation: ")
            val cmd = BufferedReader(InputStreamReader(System.`in`)).readLine().toLowerCase().firstOrNull()
            if (cmd == QUIT_CMD) return
            if (cmd == HELP_CMD)
                printHelp()
            else
                items.find { cmd == it.opCode }?.let {
                    try {
                        val shouldExit = it.action()
                        if (shouldExit)
                            return
                    } catch (e: Exception) {
                        onException(e)
                    }
                } ?: println("Command not understood")

            run(false)
        }

        run(true)
    }

    private fun printHelp() {
        headerForHelp()?.let { println(it) }
        items.forEach { println("${it.opCode}: ${it.text()}") }
        println("h: print this help")
        println("q: quit this menu")
        println("--")
    }
}

// fun main(args: Array<String>) {
//     var lap = 1
//     TextMenu(
//             TextMenuItem('1', { "$lap: Option 1" }, { println("."); lap++ }),
//             TextMenuItem('2', { "$lap: Option 2" }, { println(".."); lap++ }),
//             TextMenuItem('3', { "$lap: Option 3" }, { println("..."); lap++ }),
//             TextMenuItem('4', { "$lap: Option 4" }, { println("...."); lap++ })
//     ).run()
// }