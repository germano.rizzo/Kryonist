/*
    This file is part of Kryonist

    Kryonist is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kryonist is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kryonist.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.germanorizzo.kryonist

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.exitProcess


object GenUtils {
    private const val FNAME_LEN = 4

    fun child(base: Path, name: String): Path = Paths.get(base.toString(), name)

    fun createDirIfNotExists(base: Path, name: String) {
        val dir = child(base, name)
        if (!Files.exists(dir))
            Files.createDirectory(dir)
    }

    private val CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray()

    fun getRandomFilename(): String {
        tailrec fun getChars(num: Int, str: String): String {
            if (num == 0)
                return str
            return getChars(num - 1, str + CHARS[Krypto.getRandomInt(CHARS.indices)])
        }

        return getChars(FNAME_LEN, "")
    }

    fun formatSize(len: Long) = when {
        len <= 1024L -> "$len bytes"
        len <= 10240L -> String.format("%.2f Kb", len / 1024f)
        len <= 1048576L -> String.format("%d Kb", len / 1024)
        len <= 10485760L -> String.format("%.2f Mb", len / 1048576f)
        len <= 1073741824L -> String.format("%d Mb", len / 1048576L)
        len <= 10737418240L -> String.format("%.2f Gb", len / 1073741824f)
        else -> String.format("%d Gb", len / 1073741824L)
    }
}

object ConsoleAppUtils {
    fun askString(prompt: String): String {
        print("$prompt: ")
        return BufferedReader(InputStreamReader(System.`in`)).readLine()
    }

    fun askPassword(prompt: String = "Enter password"): String {
        val c = System.console() ?: return askString(prompt)
        return String(c.readPassword("$prompt: "))
    }

    fun askConfirmation(prompt: String): Boolean {
        print("$prompt ")
        while (true) {
            print("(Y or N): ")
            when (BufferedReader(InputStreamReader(System.`in`)).readLine()) {
                "Y", "y" -> return true
                "N", "n" -> return false
            }
        }
    }

    fun exitWithError(msg: String) {
        println(msg)
        exitProcess(-1)
    }
}

class NullOutputStream : OutputStream() {
    override fun write(b: Int) {}
}
