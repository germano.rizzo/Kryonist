/*
    This file is part of Kryonist

    Kryonist is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kryonist is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kryonist.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.germanorizzo.kryonist

import java.nio.charset.Charset
import java.security.GeneralSecurityException
import java.security.MessageDigest
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


fun String.toUTF8ByteArray() = this.toByteArray(Charset.forName("UTF-8"))

class Krypto(pwd: String) {
    companion object {
        private const val CIPHER = "AES"
        private const val CIPHER_COMPLETE = "$CIPHER/CTR/PKCS5Padding"
        private const val DIGEST = "SHA-256"
        private const val DIGEST_SIZE = 256 shr 3 // 256 bit

        const val IV_SIZE = 128 shr 3 //128 bit
        const val KEY_SIZE = 128 shr 3 //128 bit

        private val RND = SecureRandom()

        fun getDigest(): MessageDigest = MessageDigest.getInstance(DIGEST)

        fun calcSHA(bytes: ByteArray, size: Int = DIGEST_SIZE): ByteArray = getDigest().digest(bytes).copyOf(size)
        fun calcSHA(pwd: String, size: Int = DIGEST_SIZE): ByteArray = calcSHA(pwd.toUTF8ByteArray(), size)

        fun getRandomInt(r: IntRange) = synchronized(RND) { RND.nextInt(r.last - r.first + 1) + r.first }
        fun getRandomBytes(size: Int): ByteArray {
            val ret = ByteArray(size)
            synchronized(RND) { RND.nextBytes(ret) }
            return ret
        }

        fun genIV() = getRandomBytes(IV_SIZE)

        fun isCipherSupported() = try {
            Krypto("").getCipher(Cipher.ENCRYPT_MODE, ByteArray(IV_SIZE))
            true
        } catch (e: GeneralSecurityException) {
            false
        }

        fun isDigestSupported() = try {
            Krypto.getDigest()
            true
        } catch (e: GeneralSecurityException) {
            false
        }
    }

    init {
        requireNotNull(pwd, { "The password can't be empty/invalid" })
    }

    val key = calcSHA(pwd, KEY_SIZE)

    fun getCipher(mode: Int, IV: ByteArray, _key: ByteArray = key): Cipher {
        require(IV.size == IV_SIZE)
        val keySpec = SecretKeySpec(_key, CIPHER)
        val ivParameterSpec = IvParameterSpec(IV)
        val cipher = Cipher.getInstance(CIPHER_COMPLETE)
        cipher.init(mode, keySpec, ivParameterSpec)
        return cipher
    }
}
