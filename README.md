# Kryonist

**BEWARE**: this project has been superseded by [Kluis](https://github.com/proofrock/Kluis). This one will not be developed anymore; it mostly works though, so it may still be useful.

Kryonist is a small commandline application that manages collections of encrypted files.

It began mainly as an exercise in writing a Kotlin app, but for a precise use case: when adopting Backblaze B2 to store my files, I came to notice that it doesn't provide encryption for the archived files. Being a software developer, I could use a well-established, but clunky, solution, or write _my own_ clunky solution. And so, Kryonist was born! ;-)

## How it works

It works in the simplest way possible: there are two folders, one containing the encrypted files (from now on, _enc_), and one for the decrypted ones (_dec_); and a file containing the encrypted list of files, with the needed metadata (_metadata_ file). This file is also encrypted. 

Kryonist allows to encrypt files in the _dec_ folder to the _enc_ folder, and of course vice-versa to decrypt files in _enc_ to _dec_. While doing so, it updates and uses the data contained in a _metadata_ file.

It's also possible to assign a section and a subsection to each file; they are stored in the _metadata_. File integrity is checked when decoding, to ensure correctness.

### Classic use case:

I have a collections of songs files I've bought, and I want to preserve them.

- I create a folder on my Hard Disk
- I use Kryonist to inizialize the folder, creating _enc/_, _dec/_ and the empty _metadata_ file
- I copy all the files to the _dec/_ folder
- I use Kryonist to encrypt them to the _enc/_ folder
  - optionally marking them with a section/subsection, like ```My Music```-```Brit Pop```
- I store the files in _enc/_ and the metadata files to any kind of storage
  - for example, I use B2 for the encrypted files and OneDrive for the metadata

For getting the files back:

- I recreate the folder structure, with _enc/_, _dec/_ and the _metadata_ file
- I use Kryonist to get the list of the files I want
  - filenames are hidden: the encrypted files are named as three-letters strings like ```XY6```, ```W1S```, ...
- I retrieve the files from the storage, and put them to _enc/_
- I use Kryonist to decrypt them

### Usage

The commandline is very basic. Ensure to use Java 9, download the latest distribution jar and type in a CLI:

```java -jar kryonist-x.x.x.jar <path_of_root_dir>```

This will start a REPL interpreter, the inline help should be enough to get moving. It's quite basic for now, expecially in the management of labels and of the file list, but it will improve. Every suggestion is more than welcome!

## License, languages, algorithms, libraries...

Kryonist is written in Kotlin and licensed under the GPLv3. 

Files and metadata are encrypted with the standard AES algorithm, in CTR mode. Key derivation and data integrity use the also standard SHA-256.

Kryonist doesn't use any external library but the JDK (v9) standard libraries and the Kotlin runtime.

## Of course...

This software has been "alive" for a short time, so assume it _will_ have bugs. I've done my best to ensure it works, but much needs to be done - first of all, write unit tests for everything.

Also, for now, it does the bare minimum to solve my use case. Please tell me how to improve it, if you want & care. 

Thanks for reading so far!! :-)
